//
//  main.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "SumManager.h"
#import "NSObject+Sum.h"
#import "LiveManager.h"
#import "ChatManager.h"
#import "ChatModel.h"

#import "LiveSubscriber.h"
#import "LiveDetailManager.h"

//block作为参数
void test1(){
    [Person eat:^{
        NSLog(@"block作为参数");
    }];
}

//block作为参数 且有参数
void test2(){
    [Person eat2:^(NSString * _Nonnull str) {
        NSLog(@"block作为参数 且有参数%@",str);
    }];
}

//block作为参数,有参数也,有返回值
void test3(){
    [Person eat3:^NSString * _Nonnull(NSString * _Nonnull str) {
        NSString *content = [NSString stringWithFormat:@"今天吃的是%@",str];
        return content;
    }];
}

//block作为返回值
void test4(){
    [Person eat4]();
}

//block作为返回值 且带参数
void test5(){
    [Person eat5](@"香蕉");
}

//block作为返回值 且带参数 还有返回值
void test6(){
    NSString *str = [Person eat6](@"苹果");
    NSLog(@"%@",str);
}

void test7(){
    NSString *str = [Person eat7:^NSString * _Nonnull(NSString * _Nonnull msg) {
        NSLog(@"%@",msg);
        return msg;
    }](@"123");
    NSLog(@"%@",str);
}

//链式编程
void test8(){
    int result = [NSString mark_makeConstraints:^(SumManager * _Nonnull mgr) {
        mgr.add(5).add(10);
    }];
    NSLog(@"%d",result);
}

//函数式编程
void test9(){
    LiveManager *manager = [LiveManager new];
    int content = [[manager liveMethod:^int(int value) {
        value += 10;
        return value;
    }] result] ;
    NSLog(@"%d",content);
}


//第三方框架常用套路1

void test10(){
    ChatModel *chatModel = [ChatManager creatChatRoom:^ChatModel * _Nonnull(ChatModel * _Nonnull model) {
        return [ChatModel creatChatModel:^{
            NSLog(@"调用了%@",model);//可以把还原设置的事情放在同一个相关的地方
        }];
    }];
    [chatModel getRoom];
}

//第三方框架常用套路2
void test11(){
    LiveDetailManager *detailManager = [LiveDetailManager createManager:^(LiveSubscriber * _Nonnull subscriber) {
        [subscriber sendNext:@"1213"];
    }];
    
    [detailManager subscribeNext:^(id  _Nonnull x) {
        NSLog(@"%@",x);
    }];
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
//        test1();
//        test2();
//        test3();
//        test4();
//        test5();
//        test6();
//        test7();
//        test8();
//        test9();
//        test10();
        test11();
    }
    return 0;
}
