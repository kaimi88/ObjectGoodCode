//
//  AFURLSessionManager.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "AFURLSessionManager.h"

@interface AFURLSessionManager ()
@property (readwrite, nonatomic, strong) NSURLSession *session;
@end

@implementation AFURLSessionManager

//构造函数
- (instancetype)init {
    return [self initWithSessionConfiguration:nil];
}

/*
 1.初始化一个session
 2.给manager的属性设置初始值
 */
- (instancetype)initWithSessionConfiguration:(NSURLSessionConfiguration *)configuration {
    self = [super init];
    if (!self) {
        return nil;
    }

    //设置默认的configuration,配置我们的session
//    if (!configuration) {
//        configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    }
//
//    //持有configuration
//    self.sessionConfiguration = configuration;
//
//    //设置为delegate的操作队列并发的线程数量1，也就是串行队列
//    self.operationQueue = [[NSOperationQueue alloc] init];
//    self.operationQueue.maxConcurrentOperationCount = 1;
//
//    /*
//     －如果完成后需要做复杂(耗时)的处理，可以选择异步队列
//     －如果完成后直接更新UI，可以选择主队列
//     [NSOperationQueue mainQueue]
//     */
//
//    self.session = [NSURLSession sessionWithConfiguration:self.sessionConfiguration delegate:self delegateQueue:self.operationQueue];
//
//    //默认为json解析
//    self.responseSerializer = [AFJSONResponseSerializer serializer];
//
//    //设置默认证书 无条件信任证书https认证
//    self.securityPolicy = [AFSecurityPolicy defaultPolicy];
//
//#if !TARGET_OS_WATCH
//    //网络状态监听
//    self.reachabilityManager = [AFNetworkReachabilityManager sharedManager];
//#endif
//
//    //delegate= value taskid = key
//    self.mutableTaskDelegatesKeyedByTaskIdentifier = [[NSMutableDictionary alloc] init];
//
//    //使用NSLock确保线程安全
//    self.lock = [[NSLock alloc] init];
//    self.lock.name = AFURLSessionManagerLockName;
//
//
//    //异步的获取当前session的所有未完成的task。其实讲道理来说在初始化中调用这个方法应该里面一个task都不会有
//    //后台任务重新回来初始化session，可能就会有先前的任务
//    //https://github.com/AFNetworking/AFNetworking/issues/3499
//    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
//        for (NSURLSessionDataTask *task in dataTasks) {
//            [self addDelegateForDataTask:task uploadProgress:nil downloadProgress:nil completionHandler:nil];
//        }
//
//        for (NSURLSessionUploadTask *uploadTask in uploadTasks) {
//            [self addDelegateForUploadTask:uploadTask progress:nil completionHandler:nil];
//        }
//
//        for (NSURLSessionDownloadTask *downloadTask in downloadTasks) {
//            [self addDelegateForDownloadTask:downloadTask progress:nil destination:nil completionHandler:nil];
//        }
//    }];

    return self;
}
@end
