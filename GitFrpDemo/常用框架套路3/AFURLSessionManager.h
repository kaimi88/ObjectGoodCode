//
//  AFURLSessionManager.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AFURLSessionManager : NSObject
//把大量的初始化操作 放到父类里面，隐藏起来，外面无感知
- (instancetype)initWithSessionConfiguration:(NSURLSessionConfiguration *)configuration ;
@end

NS_ASSUME_NONNULL_END
