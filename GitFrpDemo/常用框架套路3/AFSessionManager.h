//
//  AFSessionManager.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLSessionManager.h"
NS_ASSUME_NONNULL_BEGIN

@interface AFSessionManager : AFURLSessionManager
+ (instancetype)manager;
@end

NS_ASSUME_NONNULL_END
