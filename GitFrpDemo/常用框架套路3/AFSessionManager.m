//
//  AFSessionManager.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "AFSessionManager.h"
@interface AFSessionManager()
@property (readwrite, nonatomic, strong) NSURL *baseURL;
@end
@implementation AFSessionManager
+ (instancetype)manager {
    //这样就可以在一个类方法里面调用对象方法，并且对外提供一个类函数
    return [[[self class] alloc] initWithBaseURL:nil];
}

- (instancetype)init {
    return [self initWithBaseURL:nil];
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    return [self initWithBaseURL:url sessionConfiguration:nil];
}

- (instancetype)initWithSessionConfiguration:(NSURLSessionConfiguration *)configuration {
    return [self initWithBaseURL:nil sessionConfiguration:configuration];
}

- (instancetype)initWithBaseURL:(NSURL *)url
           sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    //调用父类初始化方法
    self = [super initWithSessionConfiguration:configuration];//这步操作很套路这步操作很套路这步操作很套路这步操作很套路这步操作很套路
    if (!self) {
        return nil;
    }

    // Ensure terminal slash for baseURL path, so that NSURL +URLWithString:relativeToURL: works as expected
    /*
     为了确保NSURL +URLWithString:relativeToURL: works可以正确执行，在baseurlpath的最后添加‘/’
     */
    //url有值且没有‘/’,那么在url的末尾添加‘/’
    if ([[url path] length] > 0 && ![[url absoluteString] hasSuffix:@"/"]) {
        url = [url URLByAppendingPathComponent:@""];
    }
    self.baseURL = url;

    return self;
}

@end
