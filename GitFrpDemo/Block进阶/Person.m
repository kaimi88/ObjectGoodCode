//
//  Person.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "Person.h"

@implementation Person
//block作为参数
+ (void)eat:(void(^)(void))block{
    block();//没有参数这里执行的时候就不用传
}

//block作为参数且有参数
+ (void)eat2:(void(^)(NSString *))block{
    block(@"苹果");
}

//block作为参数,有参数也,有返回值
+ (void)eat3:(NSString* (^)(NSString *))block{
    NSString *content = block(@"桃子");
    NSLog(@"%@",content);
}

//block作为返回值
+ (void(^)(void))eat4{
    return ^(){
        NSLog(@"block作为返回值");
    };
}

//block作为返回值且有参数
+ (void(^)(NSString *))eat5{
    return ^(NSString *str){
        NSLog(@"block作为返回值且有参数%@",str);
        //没有返回值就啥都不用写
    };
}

//block作为返回值且有参数,且有返回值
+ (NSString *(^)(NSString *))eat6{
    return ^(NSString *str){//返回一个block     ^
        NSString *content = [NSString stringWithFormat:@"block作为返回值且有参数%@",str];
        return content;//返回block的类型是NSString，值是content
    };
}

//block 即做参数 又做返回值 综合小练习

//这个有点函数式编程的意思了，不过函数式block返回的不是NSString的block，而是自身
+ (NSString *(^)(NSString *))eat7:(NSString *(^)(NSString *))block{
    NSString *str = block(@"参数的值是1");
    NSLog(@"参数经过加工后的值是%@",str);
    return ^(NSString *msg){
        NSLog(@"msg:%@",msg);
        NSString *content = [NSString stringWithFormat:@"content把msg加工后%@%@",msg,str];
        return content;
    };
}
@end
