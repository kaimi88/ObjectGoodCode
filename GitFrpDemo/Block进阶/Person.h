//
//  Person.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject
//block作为参数
+ (void)eat:(void(^)(void))block;

//block作为参数且有参数
+ (void)eat2:(void(^)(NSString *))block;


//block作为参数,有参数也,有返回值
+ (void)eat3:(NSString* (^)(NSString *))block;


//block作为返回值

+ (void(^)(void))eat4;

//block作为返回值且有参数
+ (void(^)(NSString *))eat5;

//block作为返回值且有参数,且有返回值
+ (NSString *(^)(NSString *))eat6;

//block 即做参数 又做返回值 综合小练习
+ (NSString *(^)(NSString *))eat7:(NSString *(^)(NSString *))block;

@end

NS_ASSUME_NONNULL_END
