//
//  LiveManager.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "LiveManager.h"

@implementation LiveManager
- (instancetype)liveMethod:(int (^)(int))block{
    self->_result +=  block(self->_result);
    return self;
}

@end
