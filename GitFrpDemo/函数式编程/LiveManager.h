//
//  LiveManager.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveManager : NSObject
@property (nonatomic,assign)int result;
- (instancetype)liveMethod:(int (^)(int))block;
@end

NS_ASSUME_NONNULL_END
