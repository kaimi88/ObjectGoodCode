//
//  ChatModel.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "ChatModel.h"
#import <libkern/OSAtomic.h>
@interface ChatModel()
@property (nonatomic,copy,readonly)void *(^subscribe)(void);
@end

@implementation ChatModel

- (id)init {
    self = [super init];
    if (self == nil) return nil;

    return self;
}

- (id)initWithBlock:(void (^)(void))block {
    NSCParameterAssert(block != nil);
    self = [super init];
    if (self == nil) return nil;
    //把block 保存下来 等到合适时机在执行
    self->_subscribe = [block copy];

    return self;
}


+ (instancetype)creatChatModel:(void (^)(void))block{
    return [[self alloc] initWithBlock:block];
}

//调用该方法的时候 才真正执行block
- (void)getRoom{
    self->_subscribe();
}
@end
