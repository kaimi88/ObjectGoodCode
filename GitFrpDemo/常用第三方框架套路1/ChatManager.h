//
//  ChatManager.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatManager : NSObject

+ (ChatModel *)creatChatRoom:(ChatModel *(^)(ChatModel *))block;

@end

NS_ASSUME_NONNULL_END
