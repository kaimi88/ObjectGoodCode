//
//  ChatModel.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatModel : NSObject
@property (nonatomic,strong)NSString *name;
+ (instancetype)creatChatModel:(void (^)(void))block;

- (void)getRoom;

@end

NS_ASSUME_NONNULL_END
