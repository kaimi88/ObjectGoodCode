//
//  LiveSubscriber.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveSubscriber : NSObject
- (void)sendNext:(id)value;
- (void)sendError:(NSError *)error;
- (void)sendCompleted;

+ (instancetype)subscriberWithNext:(void (^)(id x))next error:(void (^)(NSError *__nonnull error))error completed:(void (^)(void))completed;
@end

NS_ASSUME_NONNULL_END
