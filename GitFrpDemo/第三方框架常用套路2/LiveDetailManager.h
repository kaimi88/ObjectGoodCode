//
//  LiveDetailManager.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LiveSubscriber.h"
NS_ASSUME_NONNULL_BEGIN

@interface LiveDetailManager : NSObject
+ (LiveDetailManager *)createManager:(void (^)(LiveSubscriber *subscriber))didSubscribe;
- (void)subscribeNext:(void (^)(id x))nextBlock;
@end

NS_ASSUME_NONNULL_END
