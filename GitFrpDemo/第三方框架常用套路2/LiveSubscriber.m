//
//  LiveSubscriber.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "LiveSubscriber.h"
@interface LiveSubscriber()
@property (nonatomic, copy) void (^next)(id value);//将block保存起来
@property (nonatomic, copy) void (^error)(NSError * __nonnull error);
@property (nonatomic, copy) void (^completed)(void);
@end
@implementation LiveSubscriber


+ (instancetype)subscriberWithNext:(void (^)(id x))next error:(void (^)(NSError *__nonnull error))error completed:(void (^)(void))completed {
    LiveSubscriber *subscriber = [[self alloc] init];

    subscriber->_next = [next copy];
    subscriber->_error = [error copy];
    subscriber->_completed = [completed copy];

    return subscriber;
}

- (void)sendNext:(id)value {
    @synchronized (self) {
        void (^nextBlock)(id) = [self.next copy];
        if (nextBlock == nil) return;
        nextBlock(value);
    }
}

- (void)sendError:(NSError *)e {
    @synchronized (self) {
        void (^errorBlock)(NSError *) = [self.error copy];
        if (errorBlock == nil) return;
        errorBlock(e);
    }
}

- (void)sendCompleted {
    @synchronized (self) {
        void (^completedBlock)(void) = [self.completed copy];
        if (completedBlock == nil) return;
        completedBlock();
    }
}

@end
