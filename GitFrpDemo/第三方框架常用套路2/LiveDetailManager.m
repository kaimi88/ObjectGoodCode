//
//  LiveDetailManager.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "LiveDetailManager.h"
#import "LiveSubscriber.h"
@interface LiveDetailManager()
@property (nonatomic, copy, readonly) void (^didSubscribe)(LiveSubscriber *subscriber);
@end
@implementation LiveDetailManager

+ (LiveDetailManager *)createManager:(void (^)(LiveSubscriber *subscriber))didSubscribe{
    LiveDetailManager *manager = [[self alloc] init];
    manager->_didSubscribe = [didSubscribe copy];
    return manager;
}

- (void)subscribe:(LiveSubscriber *)subscriber {
    NSCParameterAssert(subscriber != nil);
    self->_didSubscribe(subscriber);
}

- (void)subscribeNext:(void (^)(id x))nextBlock {
    NSCParameterAssert(nextBlock != NULL);
    LiveSubscriber *o = [LiveSubscriber subscriberWithNext:nextBlock error:nil completed:nil];
    return [self subscribe:o];
}
@end
