//
//  SumManager.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "SumManager.h"

@implementation SumManager
- (SumManager* (^)(int value))add{
    return ^(int value){
        self->_result += value;
        return self;
    };
}
@end
