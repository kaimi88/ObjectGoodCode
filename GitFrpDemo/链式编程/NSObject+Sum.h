//
//  NSObject+Sum.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <AppKit/AppKit.h>


#import <Foundation/Foundation.h>
#import "SumManager.h"
NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Sum)
+ (int)mark_makeConstraints:(void (^)(SumManager *))block;
@end

NS_ASSUME_NONNULL_END
