//
//  NSObject+Sum.m
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import "NSObject+Sum.h"

#import <AppKit/AppKit.h>


@implementation NSObject (Sum)
+ (int)mark_makeConstraints:(void (^)(SumManager *))block{
    SumManager * mgr = [[SumManager alloc]init];
    block(mgr);//传入想点语法的这个类也是一个关键的步骤，这样在外面的block里就可以不停的点语法了
    return mgr.result;
}
@end
