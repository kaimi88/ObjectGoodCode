//
//  SumManager.h
//  GitFrpDemo
//
//  Created by mark on 2020/3/4.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SumManager : NSObject
@property (nonatomic,assign)int result;
//想用点语法 就必须是get 方法，但是如果想传参数- (void)sum:(int)value 这样传参数就没有办法用点语法了，又想传参 又想点语法 怎么办？ 返回值是block带参数的那种就可以了啊

- (SumManager* (^)(int value))add;
@end

NS_ASSUME_NONNULL_END
